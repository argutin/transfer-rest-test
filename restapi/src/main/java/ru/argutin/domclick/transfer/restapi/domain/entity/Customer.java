package ru.argutin.domclick.transfer.restapi.domain.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private BigDecimal money;

    public Long getId() {
        return id;
    }

    public Customer setId(Long id) {
        this.id = id;
        return this;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public Customer setMoney(BigDecimal money) {
        this.money = money;
        return this;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", money=" + money +
                '}';
    }
}