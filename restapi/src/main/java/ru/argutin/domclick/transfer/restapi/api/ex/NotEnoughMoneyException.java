package ru.argutin.domclick.transfer.restapi.api.ex;

public class NotEnoughMoneyException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Not enough money";
    }
}
