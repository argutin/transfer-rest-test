package ru.argutin.domclick.transfer.restapi.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.argutin.domclick.transfer.restapi.api.ex.AccountNotFoundException;
import ru.argutin.domclick.transfer.restapi.api.ex.NotEnoughMoneyException;

@ControllerAdvice
class AccountNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(AccountNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@SuppressWarnings("unused")
	String accountNotFoundHandler(AccountNotFoundException ex) {
		return ex.getMessage();
	}

	@ResponseBody
	@ExceptionHandler(NotEnoughMoneyException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@SuppressWarnings("unused")
	String notEnoughMoney(NotEnoughMoneyException ex) {
		return ex.getMessage();
	}
}