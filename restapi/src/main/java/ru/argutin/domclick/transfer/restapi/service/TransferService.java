package ru.argutin.domclick.transfer.restapi.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.argutin.domclick.transfer.restapi.Application;
import ru.argutin.domclick.transfer.restapi.domain.dao.CustomRepository;
import ru.argutin.domclick.transfer.restapi.domain.entity.Customer;
import ru.argutin.domclick.transfer.restapi.api.ex.AccountNotFoundException;
import ru.argutin.domclick.transfer.restapi.api.ex.NotEnoughMoneyException;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class TransferService {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    private final CustomRepository customRepository;

    public TransferService(CustomRepository customRepository) {
        this.customRepository = customRepository;
    }

    @Transactional
    public void transfer(long fromAccountId, long toAccountId, BigDecimal amount) {
        Optional<Customer> fromAccountOptional = customRepository.findById(fromAccountId);
        if (!fromAccountOptional.isPresent()) {
            throw new AccountNotFoundException();
        }

        Customer fromAccount = fromAccountOptional.get();
        if (fromAccount.getMoney().compareTo(amount) < 0) {
            throw new NotEnoughMoneyException();
        }

        Optional<Customer> toAccountOptional = customRepository.findById(toAccountId);
        if (!toAccountOptional.isPresent()) {
            throw new AccountNotFoundException();
        }

        Customer toAccount = toAccountOptional.get();

        customRepository.setMoneyFor(fromAccount.getMoney().subtract(amount), fromAccount.getId());
        customRepository.setMoneyFor(toAccount.getMoney().add(amount), toAccount.getId());
    }

    @Transactional
    public void deposit(long accountId, BigDecimal amount) {
        Optional<Customer> accountOptional = customRepository.findById(accountId);
        if (!accountOptional.isPresent()) {
            throw new AccountNotFoundException();
        }

        Customer account = accountOptional.get();

        customRepository.setMoneyFor(account.getMoney().add(amount), account.getId());
    }

    @Transactional
    public void withdraw(long accountId, BigDecimal amount) {
        Optional<Customer> accountOptional = customRepository.findById(accountId);
        if (!accountOptional.isPresent()) {
            throw new AccountNotFoundException();
        }

        Customer account = accountOptional.get();

        customRepository.setMoneyFor(account.getMoney().subtract(amount), account.getId());
    }
}
