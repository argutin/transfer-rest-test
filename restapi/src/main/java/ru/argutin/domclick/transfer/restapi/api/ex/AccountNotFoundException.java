package ru.argutin.domclick.transfer.restapi.api.ex;

public class AccountNotFoundException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Account not found";
    }
}
