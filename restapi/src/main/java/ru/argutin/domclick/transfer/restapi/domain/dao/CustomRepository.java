package ru.argutin.domclick.transfer.restapi.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.argutin.domclick.transfer.restapi.domain.entity.Customer;

import javax.persistence.LockModeType;
import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface CustomRepository extends JpaRepository<Customer, Long> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Customer> findById(Long id);

    @Modifying
    @Query("update Customer c set c.money = ?1 where c.id = ?2")
    void setMoneyFor(BigDecimal amount, Long accountId);

}
