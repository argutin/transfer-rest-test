package ru.argutin.domclick.transfer.restapi.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.argutin.domclick.transfer.restapi.service.TransferService;

import java.math.BigDecimal;

@RestController
@RequestMapping("/account")
public class AccountController {

    private final TransferService transferService;

    @Autowired
    public AccountController(TransferService transferService) {
        this.transferService = transferService;
    }

    @RequestMapping(value = "/transfer", method = RequestMethod.PUT)
    public void greeting(@RequestHeader long fromAccountId, @RequestHeader long toAccountId, @RequestHeader BigDecimal amount) {
        transferService.transfer(fromAccountId, toAccountId, amount);
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.PUT)
    public void deposit(@RequestHeader long accountId, @RequestHeader BigDecimal amount) {
        transferService.deposit(accountId, amount);
    }

    @RequestMapping(value = "/withdraw", method = RequestMethod.PUT)
    public void withdraw(@RequestHeader long accountId, @RequestHeader BigDecimal amount) {
        transferService.withdraw(accountId, amount);
    }
}
