package ru.argutin.domclick.transfer.restapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ru.argutin.domclick.transfer.restapi.domain.dao.CustomRepository;
import ru.argutin.domclick.transfer.restapi.domain.entity.Customer;

import java.math.BigDecimal;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public CommandLineRunner demo(CustomRepository repository) {
		return (args) -> {
            repository.save(new Customer().setMoney(new BigDecimal(100)));
            repository.save(new Customer().setMoney(new BigDecimal(100)));
		};
	}

}